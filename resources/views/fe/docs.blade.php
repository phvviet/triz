<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <title>Docs</title>
    <link rel="stylesheet" type="text/css" href="{{asset('fe_document/fonts/font-awesome-4.3.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fe_document/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fe_document/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fe_document/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fe_document/css/custom.css')}}">
</head>
<body>
    <div id="wrapper">
        <div class="custom_container">
            <div class="ref_to_home">
                <a href="/">
                    <span>
                        <i class="fa fa-home"></i>
                    </span>
                </a>
                <a href="/login" class="sign_in">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    <span >
                         Đăng Nhập
                    </span>
                </a>
            </div>
            <section id="top" class="section docs-heading">
                <div class="row">
                    <div class="col-md-12">
                        <div class="big-title text-center">
                            <h1>Tài liệu tham khảo</h1>
                            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit.Version 1.0</p>
                        </div>
                    </div>
                </div>
                <hr>
            </section>
            <div class="row">
                <div class="col-md-3">
                    <nav class="docs-sidebar" data-spy="affix" data-offset-top="300" data-offset-bottom="200" role="navigation">
                        <ul class="nav">
                            <li><a href="/">Về lại trang chủ</a></li>
                            <li><a href="#line1">Getting Started</a></li>
                            <li><a href="#line7">How to Use Option Panel</a>
                                <ul class="nav">
                                    <li><a href="#line7_1">General Options</a></li>
                                    <li><a href="#line7_2">Style Options</a></li>
                                    <li><a href="#line7_3">Header Options</a>
                                        <ul class="nav nav_lv_2">
                                            <li><a href="#line7_3_1">General Options 1</a></li>
                                            <li><a href="#line7_3_2">Style Options 1</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav >
                </div>
                <div class="col-md-9">
                    <section class="welcome">
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h2 class="dark-text">Introduction<hr></h2>
                                <div class="row">
                                    <div class="col-md-12 full">
                                        <div class="intro1">
                                            <ul>
                                                <li><strong>Item Name : </strong>Lorem, ipsum dolor sit</li>
                                                <li><strong>Item Version : </strong> v 1.0</li>
                                                <li><strong>Author  : </strong> <a href="#" target="_blank">Lorem, ipsum dolor sit</a></li>
                                            </ul>
                                        </div>
                                        <hr>
                                        <div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quis odit quod deserunt minus amet. At facere cum, iusto recusandae, deleniti, doloremque maiores asperiores molestias incidunt magni iure voluptatem soluta.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                        <section id="line1" class="section">
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h2 class="dark-text m-bold-text">Getting Started <a href="#top">#back to top</a><hr></h2>
                            </div>
                            <!-- end col -->
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quis odit quod deserunt minus amet. At facere cum, iusto recusandae, deleniti, doloremque maiores asperiores molestias incidunt magni iure voluptatem soluta.</p>
                    </section>
                    <!-- end section -->
                    <section id="line7" class="section">
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h2 class="dark-text m-bold-text">How to Use Option Panel <a href="#top">#back to top</a><hr></h2>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h3 id="line7_1" class="dark-text">General Option <a href="#top">#back to top</a></h3>
                            </div>
                            <!-- end col -->
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis itaque voluptatum voluptates eligendi earum pariatur molestias neque, maxime accusamus? Fugiat nostrum reprehenderit nesciunt, placeat quas totam distinctio eum ipsam esse?</p>
                        </div>

                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h3 id="line7_2" class="dark-text">Style Option <a href="#top">#back to top</a></h3>
                            </div>
                            <!-- end col -->
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis itaque voluptatum voluptates eligendi earum pariatur molestias neque, maxime accusamus? Fugiat nostrum reprehenderit nesciunt, placeat quas totam distinctio eum ipsam esse?</p>
                        </div>
                        <div class="row">
                            <div class="col-md-12 left-align">
                                <h3 id="line7_3" class="dark-text m-bold-text">Header Option <a href="#top">#back to top</a><hr></h3>
                            </div>
                            <!-- end col -->
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis itaque voluptatum voluptates eligendi earum pariatur molestias neque, maxime accusamus? Fugiat nostrum reprehenderit nesciunt, placeat quas totam distinctio eum ipsam esse?</p>
                            <div class="col-md-12 left-align sub_menu">
                                <h4 id="line7_3_1" class="dark-text">General Option <a href="#top">#back to top</a></h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>

                            </div>
                            <div class="col-md-12 left-align">
                                <h4 id="line7_3_2" class="dark-text">Style Option <a href="#top">#back to top</a></h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta voluptatum ipsum rem fugiat illo, sint doloremque esse quisquam illum. Iure aliquid ipsam repellendus pariatur perferendis porro aspernatur eius! Est, voluptas!</p>
                            </div>

                        </div>
                    </section>
                    <!-- end section -->
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('fe_document/js/jquery.min.js')}}"></script>
    <script src="{{asset('fe_document/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('fe_document/js/wow.js')}}"></script>
    <!-- CUSTOM PLUGINS -->
    <script src="{{asset('fe_document/js/custom.js')}}"></script>
    <script src="{{asset('fe_document/js/main.js')}}"></script>

</body>

</html>
