@extends('layouts.home')
@section('content')
<section class="section gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-40px-r lg-p-15px-r md-m-15px-tb">
                <div class="article box-shadow">
                    <div class="article-img">
                        <img src="{{asset('homepage/img/blog-1.jpg')}}" title="" alt="">
                    </div>
                    <div class="article-title">
                        <h6><a href="#">Lifestyle</a></h6>
                        <h2>They Now Bade Farewell To The Kind But Unseen People</h2>
                        <!-- <div class="media">
                            <div class="avatar">
                                <img src="{{asset('homepage/img/team-1.jpg')}}" title="" alt="">
                            </div>
                            <div class="media-body">
                                <label>Rachel Roth</label>
                                <span>26 FEB 2020</span>
                            </div>
                        </div> -->
                    </div>
                    <div class="article-content">
                        <p>Aenean eleifend ante maecenas pulvinar montes lorem et pede dis dolor pretium donec dictum. Vici consequat justo enim. Venenatis eget adipiscing luctus lorem. Adipiscing veni amet luctus enim sem libero tellus viverra venenatis aliquam. Commodo natoque quam pulvinar elit.</p>
                        <p>Eget aenean tellus venenatis. Donec odio tempus. Felis arcu pretium metus nullam quam aenean sociis quis sem neque vici libero. Venenatis nullam fringilla pretium magnis aliquam nunc vulputate integer augue ultricies cras. Eget viverra feugiat cras ut. Sit natoque montes tempus ligula eget vitae pede rhoncus maecenas consectetuer commodo condimentum aenean.</p>
                        <h4>What are my payment options?</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                            <p class="blockquote-footer">Someone famous in <cite title="Source Title">Dick Grayson</cite></p>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>
@endsection
