@extends('layouts.home')
@section('content')
<!-- Section -->
<section class="section">
    <div class="container">
        <div class="row md-m-25px-b m-45px-b justify-content-center text-center">
            <div class="col-lg-8">
                <h3 class="h1 m-20px-b p-20px-b theme2nd-after after-50px">Giải pháp tốt nhất cho bạn</h3>
                <p class="m-0px font-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel delectus quaerat temporibus accusamus totam praesentium</p>
            </div>
        </div>
        <div class="tab-style-4">
            <ul class="nav nav-fill nav-tabs">
                <li class="nav-item">
                    <a href="#tab3_sec1" data-toggle="tab" class="active">
                        <div class="icon"><i class="icon-chat"></i></div>
                        <span>Discussion</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#tab3_sec2" data-toggle="tab">
                        <div class="icon"><i class="icon-tools"></i></div>
                        <span>Creative Concept</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#tab3_sec3" data-toggle="tab">
                        <div class="icon"><i class="icon-megaphone"></i></div>
                        <span>Production</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab3_sec1" class="tab-pane fade in active show">
                    <div class="row align-items-center justify-content-between p-25px-t md-p-15px-t">
                        <div class="col-lg-6 text-center">
                            <img src="{{asset('homepage/img/ai-4.svg')}}" title="" alt="">
                        </div>
                        <div class="col-lg-5">
                            <h2 class="h2 m-15px-b">Welcome to Raino Digital Marketing</h2>
                            <p class="m-25px-b">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                            <div class="media m-30px-b">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-desktop"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Web Development</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-tools"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Logo & Identity</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab3_sec2" class="tab-pane fade in">
                    <div class="row align-items-center justify-content-between p-25px-t md-p-15px-t">
                        <div class="col-lg-5">
                            <h2 class="h2 m-15px-b">Welcome to Raino Digital Marketing</h2>
                            <p class="m-25px-b">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                            <div class="media m-30px-b">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-desktop"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Web Development</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-tools"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Logo & Identity</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 text-center">
                            <img src="{{asset('homepage/img/ai-4.svg')}}" title="" alt="">
                        </div>
                    </div>
                </div>
                <div id="tab3_sec3" class="tab-pane fade in">
                    <div class="row align-items-center justify-content-between p-25px-t md-p-15px-t">
                        <div class="col-lg-6 text-center">
                            <img src="{{asset('homepage/img/ai-4.svg')}}" title="" alt="">
                        </div>
                        <div class="col-lg-5">
                            <h2 class="h2 m-15px-b">Welcome to Raino Digital Marketing</h2>
                            <p class="m-25px-b">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                            <div class="media m-30px-b">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-desktop"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Web Development</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="icon-60 dots-icon border-radius-5 border-color-theme2nd border-all-1 theme2nd-color">
                                    <i class="icon-tools"></i>
                                    <span class="dots"><i class="dot dot1"></i><i class="dot dot2"></i><i class="dot dot3"></i></span>
                                </div>
                                <div class="media-body p-20px-l">
                                    <h6>Logo & Identity</h6>
                                    <p class="m-0px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section -->
<!-- Section -->
<section class="section">
    <div class="container">
        <div class="row md-m-25px-b m-40px-b justify-content-center">
            <div class="col-lg-8 text-center">
                <h3 class="h1 m-15px-b p-15px-b after-50 theme2nd-after">Sản phẩm của chúng tôi</h3>
            </div>
        </div>
        <div class="row p-40px-tb">
            <div class="col-lg-6 m-15px-tb">
                <h3 class="h1">Product A</h3>
                <p class="font-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis officia illum aliquid repellat quod neque.</p>
                <ul class="list-type-03 theme2nd">
                    <li>
                        <h6>Built with customization</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                    <li>
                        <h6>Quality design and thoughfully</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                    <li>
                        <h6>Quality design and thoughfully</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <img class="max-width-120" src="{{asset('homepage/img/ai-3.png')}}" title="" alt="">
            </div>
            <div class="p-20px-t m-auto-lr">
                <a class="m-btn m-btn-radius m-btn-theme2nd" href="#">
                    <span class="m-btn-inner-text">Chi tiết</span>
                    <span class="m-btn-inner-icon arrow"></span>
                </a>
            </div>
        </div>
        <div class="border-bottom-3 border-color-dark-gray"></div>
        <div class="row p-40px-tb">
            <div class="col-lg-6 m-15px-tb">
                <img class="max-width-100" src="{{asset('homepage/img/ai-3.png')}}" title="" alt="">
            </div>
            <div class="col-lg-6 m-15px-tb">
                <h3 class="h1">Product A</h3>
                <p class="font-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis officia illum aliquid repellat quod neque.</p>
                <ul class="list-type-03 theme2nd">
                    <li>
                        <h6>Built with customization</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                    <li>
                        <h6>Quality design and thoughfully</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                    <li>
                        <h6>Quality design and thoughfully</h6>
                        <p>Get the ultimate tool and learn how to grow your audience and build an online business.</p>
                    </li>
                </ul>
            </div>
            <div class="p-20px-t m-auto-lr">
                <a class="m-btn m-btn-radius m-btn-theme2nd" href="#">
                    <span class="m-btn-inner-text">Chi tiết</span>
                    <span class="m-btn-inner-icon arrow"></span>
                </a>
            </div>

        </div>
    </div>
</section>
<!-- End Section -->
@endsection
