<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\UserController;

Route::get('/', [FrontEndController::class,'homepage']);
Route::get('/contact', [FrontEndController::class,'contact']);
Route::get('/about', [FrontEndController::class,'about']);
Route::get('/service', [FrontEndController::class,'service']);
Route::get('/document',[FrontEndController::class,'pDocument']);
Route::get('/triz/introduce',[FrontEndController::class,'trizIntroduce']);

Route::group([ "middleware" => ['auth:sanctum', 'verified'] ], function() {
    Route::view('admin/dashboard', "dashboard")->name('dashboard');

    Route::get('admin/user', [ UserController::class, "index_view" ])->name('user');
    Route::view('admin/user/new', "pages.user.user-new")->name('user.new');
    Route::view('admin/user/edit/{userId}', "pages.user.user-edit")->name('user.edit');
});
