<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    //
    public function homepage(){
        return view('fe.homepage')->with([
            'homepage'=>true
        ]);
    }
    public function contact(){
        return view('fe.contact')->with([
            'contact' => true
        ]);
    }
    public function about(){
        return view('fe.about')->with([
            'about' => true
        ]);
    }
    public function service(){
        return view('fe.service')->with([
            'service' => true
        ]);
    }
    public function pDocument(){
        return view('fe.docs')->with([
            'doc' => true
        ]);
    }
    public function trizIntroduce(){
        return view('fe.intro')->with([
            'triz' => true
        ]);
    }
}
